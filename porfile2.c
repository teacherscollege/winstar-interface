/*TMCI18N BEGIN HEADER*/
#if !defined( tmBundle_EXISTS )
/* Global Init via linkage with tminit.cpp generated object required */
#include "tmcilib.h"
static struct TMBundle tmBundle = {"porfile2.c",NULL,NULL,NULL,NULL};
#define tmBundle_EXISTS
#endif

#if !defined( tmporfile_EXISTS )
/* Global Init via linkage with tminit.cpp generated object required */
#include "tmcilib.h"
static struct TMBundle tmporfile = {"porfile2.c",NULL,NULL,NULL,NULL};
#define tmporfile_EXISTS
#endif

/*TMCI18N END HEADER*/

/* AUDIT_TRAIL_MSGKEY_UPDATE
-- PROJECT : MSGKEY
-- MODULE  : PORFILE
-- SOURCE  : enUS
-- TARGET  : I18N
-- DATE    : Mon Oct 26 00:02:42 2015
-- MSGSIGN : #2e66b3e6d3aa6be6
END AUDIT_TRAIL_MSGKEY_UPDATE */

/* AUDIT_TRAIL_TM63
-- TranMan 6.3
-- PROJECT : HEGDE_I18N
-- MODULE  : PORFILE
-- SOURCE  : enUS
-- TARGET  : I18N
-- DATE    : Tue Oct 09 05:53:49 2007
END AUDIT_TRAIL_TM63 */

/******************************************************************************/
/*                                                                            */
/* Copyright 2002 - 2015 Ellucian Company L.P. and its affiliates.            */
/*                                                                            */
/******************************************************************************/

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */
/*                                                                            */
/*   Program : PORFILE2.C                                                      */
/*                                                                            */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */
/*                                                                            */
/*                                                                            */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */
/*                                                                            */
/*                                                                            */
/* AUDIT TRAIL:  6.0                                                          */
/* 1. Initial Coding HR.                           MT, LN  16-Dec-2002        */
/*                                                                            */
/* AUDIT TRAIL: 6.1                                      LN  11/06/2003       */
/* 1. Defect 61-0009                                                          */
/*    Problem:                                                                */
/*      The process doesn't handle an input field with an embedded apostrophe.*/
/*    Functional Impact:                                                      */
/*      The process will handle an input field with an embedded apostrophe.   */
/*    Technical Fix:                                                          */
/*      Addded function replace_char to replace a single apostrophe with two  */
/*      apostrophes.                                                          */
/*                                                                            */
/* AUDIT TRAIL: 7.0                                                           */
/* 1. Changes Migrated to Release 7.0                  LN   07/15/2004        */
/*    Defect# 61-0009  Release 6.1                                            */
/*                                                                            */
/* AUDIT TRAIL: 7.1                                                           */
/* 1. Defect# 71-0002                                  LN   01/19/2005        */
/*    Removed unused variables.                                               */
/*                                                                            */
/* AUDIT TRAIL: 7.2.0.1                                                       */
/* 1. RPE : 1-NCB8R,1-N4ZGT                                                   */
/*    Modified the fill_blanks routine to avoid the compilation warnings.     */
/*                                                                            */
/* AUDIT TRAIL: 8.0                                                           */
/*                                                                            */
/* AUDIT TRAIL: 8.4.2                                                         */
/* 1. Defect# 1-IOL5CK                                                        */
/*    Problem:                                                                */
/*      Incorrect message is printed in the xpdp1042.pc.                      */
/*    Functional Impact:                                                      */
/*      Correct message is printed in the xpdp1042.pc.                        */
/*    Technical Fix:                                                          */
/*      Added tmporfile variable for library string messages.                 */
/*                                                                            */
/*
/* AUDIT TRAIL: 8.9.0.5
   1. Defect# 1-12QKCFM                                SL 05/10/2013
      Problem: XPDP1042 process was not processing if a payment.dat file 
        contained apostrophes in the last name field.  The report completes 
        and creates a core dump file.
      Functional Impact: Process will not abort with error.
      Technical Fix: To prevent from running into issues due to special 
        characters embedded in names from payment flat file, the routine 
        do_get_values is modified to null out first, middle and last names 
        from payment file since names are not used in loading information 
        into Banner tables and spriden names are used in the xpdp1042 process.
*/
/*                                                                            */
/* AUDDIT TRAIL: 8.11.5.2                              AHP 06/19/2015         */
/* 1. CR-000130310                                                            */     
/*    Increased the MAX_REC_LEN from 1000 to 1200 due to the new increased    */
/*    record length in the payment.dat file.                                  */
/*
   AUDIT TRAIL: 8.12.1.5
   1. Defect# CR-000135191                             SL 10/24/2015
      Problem: XPDP1042 process terminates with memory fault if FICA.dat
        contains ' and space in a name.
      Functional Impact: Process will not abort with memory error.
      Technical Fix: To prevent from running into memory fault issue due to
        last, first or middle name embedding ' and space in payment.dat or
        FICA.dat flat files, rewrote replace_char routine to replace ' and space
        with - without using recursive tmmemset function calls. 
*/
/*  AUDIT TRAIL END                                                           */
/*                                                                            */
/* Tables:                                                                    */
/*                                                                            */
/*     Tables:(Owner.tablename) The tables used are:                          */
/*                                                                            */
/*             ptrfile                                                        */

/*#define SCT_DEBUG             1  */
#define MAX_REC_LEN           1400
#define MAX_STR_LEN           3000
#define YES                      1
#define NO                       0

struct FileType {
	UFILE *stream;
	int  recLen;
	TMCHAR *str;
};

struct ColMap {
	TMCHAR  cName[31];
	int   cPos;
        int   cLen;
	TMCHAR  cDataType[11];
        TMCHAR  cRequired[2];
  struct ColMap *next;
};

EXEC SQL BEGIN DECLARE SECTION;
static TMCHAR rpt_col_id[90]={0};
static int    rpt_col_pos=0;
static int    rpt_col_length=0;
static TMCHAR  rpt_data_type[11]={0}/*TMCI18N CHANGED FROM ""*/;
static TMCHAR  rpt_col_required[2]={0}/*TMCI18N CHANGED FROM ""*/;
static int    rpt_col_offset=0;
static int    prev_col_offset=1;

static TMCHAR rpt_process_id[10]={0}/*TMCI18N CHANGED FROM ""*/;
static TMCHAR rpt_record_id[10]={0}/*TMCI18N CHANGED FROM ""*/;
static int    rpt_rec_length=0;
static TMCHAR  temp_string[50]={0}/*TMCI18N CHANGED FROM ""*/;
static TMCHAR  temp_string1[50]={0}/*TMCI18N CHANGED FROM ""*/;

short Ind_h_01;
short Ind_h_02;
short Ind_h_03;
short Ind_h_04;
short Ind_h_05;
short Ind_h_06;
short Ind_h_07;
short Ind_h_08;

short Ind_01;
short Ind_02;
short Ind_03;
short Ind_04;
short Ind_05;

EXEC SQL END DECLARE SECTION;

TMCHAR *rec_buffer;
TMCHAR **value_array;
int index_value;
int col_len;
TMCHAR errmsg[133];

/* struct variables */
struct ColMap *aMapList;
static void create_record(TMCHAR **val_arr, TMCHAR *rpt_name, TMCHAR *rec_id, int record_length, int cols_printed, int col_length, TMCHAR record_buffer[]);
static void create_csv_record(TMCHAR **val_arr, TMCHAR *rpt_name, TMCHAR *rec_id, int record_length, int cols_printed, TMCHAR record_buffer[]);
static void fill_blanks(TMCHAR *input_string, long field_length, TMCHAR *f_type);
static int get_record_length(TMCHAR process_id[31], TMCHAR rec_id[3]);
static int write_flat_file(UFILE *file_ptr, TMCHAR *record_buffer);
static int sel_cols(int mode);
static void cols_body(void);


/* Function prototypes */
static int    open_stream(TMCHAR *aFileName, struct FileType **);
static void   close_stream( struct FileType **);
static int    get_str_len(UFILE *);
static void   load_map_list(TMCHAR[31], TMCHAR *[], struct ColMap **);
static void   traverse_map_list(struct ColMap *);
static void   del_map_list(struct ColMap **);
static void   do_get_columns(struct ColMap *, TMCHAR *);
static int    do_get_values(struct ColMap *, TMCHAR *, TMCHAR *);
static int    load_file(TMCHAR *, TMCHAR *, TMCHAR *[], TMCHAR *);
static int    do_import(TMCHAR *, TMCHAR *);
static TMCHAR*  trim_space(TMCHAR *);
static TMCHAR*  replace_char(TMCHAR *sourceStr);
static TMCHAR*  sct_itoa(int);
static int    isint(TMCHAR *);
static int    isnum(TMCHAR *);
static int    isdate(TMCHAR *);
static int    datecmp(TMCHAR *, TMCHAR *);


/* ************************************************************************** */
static void create_record(TMCHAR **val_arr, TMCHAR *rpt_name, TMCHAR *rec_id, int record_length, int cols_printed, int col_length, TMCHAR record_buffer[])
{
    prev_col_offset=1;
    index_value=0;
    rec_buffer = record_buffer;
    tmmemset(rec_buffer,' ',record_length);
    rec_buffer[record_length-1]='\0';
    value_array = val_arr;
    col_len=col_length;
    tmstrcpy(rpt_process_id,rpt_name);
    tmstrcpy(rpt_record_id,rec_id);
    report(sel_cols, cols_body,NULL,NULL);
    tmstrcpy(record_buffer,rec_buffer);
}

static void cols_body(void)
{
    if (compare(rpt_col_required,_TMC("Y"),EQS))
      {
        if (prev_col_offset == rpt_col_pos )
            {
              fill_blanks(value_array[index_value],rpt_col_length,rpt_data_type);
              tmmemcpy(rec_buffer + (rpt_col_pos-1),value_array[index_value],rpt_col_length);
            }
        else
            {
              tmmemset(rec_buffer + (prev_col_offset-1),' ',rpt_col_pos-prev_col_offset);
              fill_blanks(value_array[index_value],rpt_col_length,rpt_data_type);
              tmmemcpy(rec_buffer + (rpt_col_pos-1),value_array[index_value],rpt_col_length);
            }
      }
    else
        tmmemset(rec_buffer + (prev_col_offset-1),' ',(rpt_col_pos-prev_col_offset)+rpt_col_length);

    index_value =index_value+1;
    prev_col_offset = rpt_col_offset;
}


static void create_csv_record(TMCHAR **val_arr, TMCHAR *rpt_name, TMCHAR *rec_id, int record_length, int cols_printed, TMCHAR record_buffer[])
{
    int numcols;

    
    prev_col_offset=1;
    
    rec_buffer = record_buffer;
    tmmemset(rec_buffer,' ',record_length);
    rec_buffer[record_length-1]='\0';
    value_array = val_arr;
    col_len=0;
    tmstrcpy(rpt_process_id,rpt_name);
    tmstrcpy(rpt_record_id,rec_id);



   for (numcols = 0; numcols < cols_printed; numcols++)
     {  col_len=tmstrlen(value_array[numcols])+1;
        tmmemset(rec_buffer + prev_col_offset,' ',record_length-prev_col_offset);    
        tmstrcpy(temp_string,value_array[numcols]);        
        tmmemmove(rec_buffer + (prev_col_offset),temp_string,col_len);       
        prev_col_offset=prev_col_offset+col_len+1; 

      }  


    tmstrcpy(record_buffer,rec_buffer);
}



static int sel_cols(int mode)
{
 EXEC SQL DECLARE cursor_cols CURSOR FOR
    SELECT PTRFILE_COL_NAME,
           PTRFILE_POS,
           PTRFILE_LENGTH,
           PTRFILE_DATA_TYPE,
           PTRFILE_REQUIRED_IND,
           PTRFILE_POS+PTRFILE_LENGTH
      FROM PTRFILE
     WHERE PTRFILE_PROCESS_ID = :rpt_process_id
       AND PTRFILE_RECORD_ID = :rpt_record_id
  ORDER BY PTRFILE_POS;

  if ( mode==CLOSE_CURSOR )
    {
      EXEC SQL CLOSE cursor_cols;
      POSTORA;
      return TRUE;
  }

  if ( mode==FIRST_ROW )
    {
      EXEC SQL OPEN cursor_cols;
      POSTORA;
    }

  EXEC SQL FETCH cursor_cols INTO
       :rpt_col_id:Ind_h_01,
       :rpt_col_pos:Ind_h_02,
       :rpt_col_length:Ind_h_03,
       :rpt_data_type:Ind_h_04,
       :rpt_col_required:Ind_h_05,
       :rpt_col_offset:Ind_h_06;
  POSTORA;

  if ( NO_ROWS_FOUND )
    {
       *rpt_col_id='\0';
       rpt_col_pos=0;
       rpt_col_length=0;
       *rpt_data_type='\0';
       *rpt_col_required='\0';
       rpt_col_offset=0;
      return FALSE;
    }

  return TRUE;
}

static void fill_blanks(TMCHAR *input_string, long field_length, TMCHAR *f_type)
{
  long  i, j;
  long str_length;

  tmmemset(temp_string, ' ',  tmcharsizeof(temp_string));
  tmmemset(temp_string1, '0', tmcharsizeof(temp_string1));

  str_length = tmstrlen(input_string);

  if (compare(f_type,_TMC("C"), EQS)) {
    for (i = 0; i < str_length; i++)
      temp_string[i] = input_string[i];

    temp_string[field_length]='\0';
    tmstrcpy(input_string, temp_string);
  }
  else {
    i = field_length - str_length;
    j = 0;

    if (tmstrchr(input_string, '-')) {
      /* Negative number format: negative sign, leading zeros, dollar amount */
      temp_string1[0] = '-';
      i++;
      j++;
    }

    for (i=i, j=j; j < str_length; i++, j++)
      temp_string1[i] = input_string[j];

    temp_string1[field_length]='\0';
    tmstrcpy(input_string, temp_string1);
  }
}

static int get_record_length(TMCHAR process_id[31], TMCHAR rec_id[3])
{
  TMCHAR lv_process_id[31]={0};
  TMCHAR lv_rec_id[3]={0};
  tmstrcpy(lv_process_id, process_id);
  tmstrcpy(lv_rec_id, rec_id);

  EXEC SQL DECLARE cursor_length CURSOR FOR
    SELECT MAX(PTRFILE_POS+PTRFILE_LENGTH)
        FROM PTRFILE
        WHERE PTRFILE_PROCESS_ID=:lv_process_id
          AND PTRFILE_RECORD_ID=:lv_rec_id;

  EXEC SQL OPEN cursor_length;
  POSTORA;

  EXEC SQL FETCH cursor_length INTO
       :rpt_rec_length:Ind_h_01;
  POSTORA;

  if ( NO_ROWS_FOUND )
    {
       rpt_rec_length=0;
    }

  return rpt_rec_length;
}

static int write_flat_file(UFILE *file_ptr, TMCHAR *record_buffer)
{
   return tmfprintf(&tmBundle, file_ptr, _TMC("{0}\n"), record_buffer);
}

/* ************************************************************************** */
static int open_stream(TMCHAR *file_name,  struct FileType **aPtr) {
	(*aPtr) = (struct FileType *) malloc(sizeof(struct FileType));
  (*aPtr)->stream = tmfopen(&tmBundle, file_name, _TMC("r"));
  if ((*aPtr)->stream == NULL) {
    tmprintf(&tmporfile, TM_NLS_Get("0000","*ERROR* Unable to open file {0}.\n"), file_name);
    return NO;
  }
  else {
    (*aPtr)->recLen = MAX_REC_LEN;

    (*aPtr)->str = (TMCHAR *) malloc(((*aPtr)->recLen + 1) * tmcharsizeof(TMCHAR));
#ifdef SCT_DEBUG
  tmprintf(&tmBundle, _TMC("open_stream file name [{0}], (*aPtr)->str) <{1}>\n"), file_name, (*aPtr)->str);
  tmfflush(tmstdout);
#endif
  }

  return YES;
}
/* ************************************************************************** */
static void close_stream(struct FileType **aPtr) {
#ifdef SCT_DEBUG
  tmprintf(&tmBundle, _TMC("close_stream ...\n"));
  tmfflush(tmstdout);
#endif
  tmfclose((*aPtr)->stream);
  free((*aPtr)->str);
  (*aPtr)->str = NULL;
  free(*aPtr);
  (*aPtr) = NULL;
}
/* ************************************************************************** */
static int get_str_len(UFILE *aStream) {
  TMCHAR *aPtr = NULL;
  int   strLen;

  tmfseek(aStream, 0, SEEK_SET);
  aPtr = (TMCHAR *) malloc(MAX_REC_LEN  * tmcharsizeof(TMCHAR));
  tmmemset(aPtr,'\0',tmcharsizeof(aPtr));

  tmfgets(aPtr, MAX_REC_LEN, aStream);
  strLen = tmstrlen(aPtr);
#ifdef SCT_DEBUG
  tmprintf(&tmBundle, _TMC("get_str_len aPtr <{0}>, len = [{1,number,integer}]\n"), aPtr, strLen);
  tmfflush(tmstdout);
#endif

  free(aPtr);
  aPtr = NULL;

  return strLen;
}
/* ************************************************************************** */
static void load_map_list(TMCHAR process_id[31], TMCHAR *recIdList[],
                            struct ColMap **aList) {
  TMCHAR8 stmt_c[3000];
  TMCHAR  aName[31] = {0}/*TMCI18N CHANGED FROM ""*/,
          idStr[100]= {0}, *sqlstm = NULL;
  int   i = 0, aPos = 0, aLen = 0, arrayPos = 0;
  TMCHAR  aDataType[11] = {0}/*TMCI18N CHANGED FROM ""*/,
        aRequired[2] = {0}/*TMCI18N CHANGED FROM ""*/;
  struct ColMap *aPtr = NULL, *tempPtr = NULL, *head = NULL, *curr = NULL;

  TMCHAR8 lv_process_id_8[31];
  tmstrcpy8(lv_process_id_8,tmtochar8(process_id));

  /* Prepare the statement */
	/* idStr = (TMCHAR*) malloc(50  * tmcharsizeof(TMCHAR));
  tmmemset(idStr,'\0',tmcharsizeof(idStr));
  */

#ifdef SCT_DEBUG
  tmprintf(&tmBundle, _TMC("load_map_list idStr <{0}>\n"), idStr);
  tmfflush(tmstdout);
#endif

  arrayPos = 0;
	while (*recIdList[arrayPos]) {
#ifdef SCT_DEBUG
  tmprintf(&tmBundle, _TMC("idStr[{0,number,integer}] = <{1}>\n"), 	arrayPos,	recIdList[arrayPos]);
  tmfflush(tmstdout);
#endif
		if (i == 0)
			tmstrcat(tmstrcat(tmstrcat(idStr, _TMC("'")), recIdList[arrayPos]), _TMC("'"));
		else
			tmstrcat(tmstrcat(tmstrcat(idStr, _TMC(", '")), recIdList[arrayPos]), _TMC("'"));

		i++;
		arrayPos++;
	}

	sqlstm = (TMCHAR*) malloc(MAX_STR_LEN * tmcharsizeof(TMCHAR));
  tmmemset(sqlstm,'\0',sizeof(sqlstm));

	tmstrcpy(sqlstm, _TMC(" SELECT PTRFILE_COL_NAME,"));
  tmstrcat(sqlstm,        _TMC(" PTRFILE_POS,"));
  tmstrcat(sqlstm,        _TMC(" PTRFILE_LENGTH,"));
  tmstrcat(sqlstm,        _TMC(" PTRFILE_DATA_TYPE,"));
  tmstrcat(sqlstm,        _TMC(" PTRFILE_REQUIRED_IND"));
  tmstrcat(sqlstm, _TMC(" FROM   PTRFILE"));
  tmstrcat(sqlstm, _TMC(" WHERE  PTRFILE_PROCESS_ID = :1"));
  tmstrcat(sqlstm,   _TMC(" AND  PTRFILE_RECORD_ID IN ("));
  tmstrcat(sqlstm, idStr);
	tmstrcat(sqlstm, _TMC(")") );
  tmstrcat(sqlstm, _TMC(" ORDER BY PTRFILE_POS"));
  tmstrcat(sqlstm, _TMC("\0"));

#ifdef SCT_DEBUG
  tmprintf(&tmBundle, _TMC("idStr = <{0}>\n"), idStr);
	tmprintf(&tmBundle, _TMC("sqlstm = <{0}>\n"), sqlstm);
  tmfflush(tmstdout);
#endif

  tmstrcpy8(stmt_c,tmtochar8(sqlstm));
EXEC SQL PREPARE S1 FROM :stmt_c;
  POSTORA;

  EXEC SQL DECLARE ptrfile_cursor CURSOR FOR S1;
  POSTORA;

  EXEC SQL OPEN ptrfile_cursor USING :lv_process_id_8;
  POSTORA;

  EXEC SQL FETCH ptrfile_cursor INTO
     :aName:Ind_01,
     :aPos:Ind_02,
     :aLen:Ind_03,
     :aDataType:Ind_04,
     :aRequired:Ind_05;
  POSTORA;

  if (!(NO_ROWS_FOUND)) {
    if ((aPtr = (struct ColMap *) malloc(sizeof(struct ColMap))) != 0) {
#ifdef SCT_DEBUG
  tmprintf(&tmBundle, _TMC("aPtr <{0}>\n"), aPtr);
  tmfflush(tmstdout);
#endif

      tmstrcpy(aPtr->cName, trim_space(aName));
      tmstrcat(aPtr->cName, _TMC("\0"));

      aPtr->cPos = aPos - 1;
      aPtr->cLen = aLen;

      tmstrcpy(aPtr->cDataType, trim_space(aDataType));
      tmstrcat(aPtr->cDataType, _TMC("\0"));

      tmstrcpy(aPtr->cRequired, trim_space(aRequired));
      tmstrcat(aPtr->cRequired, _TMC("\0"));

      aPtr->next = NULL;

      head = aPtr;
      curr = head;
    }
  }

  while (ROWS_FOUND) {
    EXEC SQL FETCH ptrfile_cursor INTO
      :aName:Ind_01,
      :aPos:Ind_02,
      :aLen:Ind_03,
      :aDataType:Ind_04,
      :aRequired:Ind_05;
    POSTORA;

    if (!(NO_ROWS_FOUND)) {
      if ((tempPtr = (struct ColMap *) malloc(sizeof(struct ColMap))) != 0) {
        tmstrcpy(tempPtr->cName, trim_space(aName));
        tmstrcat(tempPtr->cName, _TMC("\0"));

        tempPtr->cPos = aPos - 1;
        tempPtr->cLen = aLen;

        tmstrcpy(tempPtr->cDataType, trim_space(aDataType));
        tmstrcat(tempPtr->cDataType, _TMC("\0"));

        tmstrcpy(tempPtr->cRequired, trim_space(aRequired));
        tmstrcat(tempPtr->cRequired, _TMC("\0"));

        tempPtr->next = NULL;

        curr->next = tempPtr;
        curr = tempPtr;
      }
    }
  }

  EXEC SQL CLOSE ptrfile_cursor;
  POSTORA;

  *aList = head;
/*  free(idStr);  */
  *idStr = '\0';

  free(sqlstm);
  sqlstm = '\0';
}
/* ************************************************************************** */
void traverse_map_list(struct ColMap *aList) {
  struct ColMap *curr = NULL;

  tmprintf(&tmBundle, _TMC("traverse_map_list ...\n"));

  curr = aList;
  while (curr) {
    tmprintf(&tmBundle, _TMC("traverse_map_list = {0}\t\n"), curr->cName);
    curr = curr->next;
  }
  tmprintf(&tmBundle, _TMC("done traverse_map_list ...\n"));
}
/* ************************************************************************** */
static void del_map_list(struct ColMap **aList) {
  struct ColMap *curr = NULL;

#ifdef SCT_DEBUG
  tmprintf(&tmBundle, _TMC("del_map_list ...\n"));
  tmfflush(tmstdout);
#endif

  curr = *aList;
  while (curr) {
    curr = (*aList)->next;
    free(*aList);
    (*aList) = curr;
  }

  curr = NULL;
#ifdef SCT_DEBUG
  tmprintf(&tmBundle, _TMC("del_map_list END ...\n"));
  tmfflush(tmstdout);
#endif
}
/* ************************************************************************** */
static void do_get_columns(struct ColMap *aList, TMCHAR *result) {
  int i = 0;
  struct ColMap *curr = NULL;

#ifdef SCT_DEBUG
  tmprintf(&tmBundle, _TMC("do_get_columns ...\n"));
  tmfflush(tmstdout);
#endif

  curr = aList;
  while (curr) {
#ifdef SCT_DEBUG
  tmprintf(&tmBundle, _TMC("inside loop curr= {0}\n"), curr->cName);
  tmfflush(tmstdout);
#endif
    if (i > 0)
      result = tmstrcat(result, _TMC(","));

    tmstrcat(result, curr->cName);
    i++;
    curr = curr->next;
  }

#ifdef SCT_DEBUG
  tmprintf(&tmBundle, _TMC("end of loop\n"));
  tmfflush(tmstdout);
#endif
}
/* ************************************************************************** */
static int do_get_values(struct ColMap *aList, TMCHAR *aRec, TMCHAR *result) {
  int i = 0, pos, retVal = 1;
  TMCHAR *aStr = NULL;
  struct ColMap *curr = NULL;

  aStr = (TMCHAR *) malloc(MAX_REC_LEN * tmcharsizeof(TMCHAR));
  tmmemset(aStr,'\0',tmcharsizeof(aStr));

#ifdef SCT_DEBUG
  tmprintf(&tmBundle, _TMC("do_get_values aStr <{0}>\n"), aStr);
  tmfflush(tmstdout);
#endif

  *errmsg = '\0';

  curr = aList;
  while (curr) {
    if (i > 0)
      result = tmstrcat(result, _TMC(","));

    pos = curr->cPos;
    /* printf("cPos = %d cLen = %d\n", pos, curr->cLen); */

    tmstrncpy(aStr, &aRec[pos], curr->cLen);
    tmstrcpy(&aStr[curr->cLen], _TMC("\0"));

#ifdef SCT_DEBUG
  tmprintf(&tmBundle, _TMC("aStr <{0}>\n"), aStr);
  tmfflush(tmstdout);
#endif

    trim_space(aStr);

    /* Pad single quote character */
    if (tmstrcmp(curr->cName,_TMC("PDR1042_LAST_NAME"))== 0||
       tmstrcmp(curr->cName,_TMC("PDR1042_FIRST_NAME"))== 0 ||
       tmstrcmp(curr->cName,_TMC("PDR1042_MI"))== 0 || 
       tmstrcmp(curr->cName,_TMC("PDTFICA_LAST_NAME"))== 0||
       tmstrcmp(curr->cName,_TMC("PDTFICA_FIRST_NAME"))== 0 ||
       tmstrcmp(curr->cName,_TMC("PDTFICA_MI"))== 0 )  {

       tmstrcpy(aStr, replace_char(aStr));

#ifdef SCT_DEBUG
  tmprintf(&tmBundle, _TMC("###aStr Space with - <{0}> {1}\n"), aStr, curr->cName);
  tmfflush(tmstdout);
#endif
    }

    if (tmstrcmp(curr->cDataType, _TMC("INTEGER")) == 0)
       {
          ltrim(aStr, _TMC("0"));
          #ifdef SCT_DEBUG
              tmprintf(&tmBundle, _TMC("aStr before validate INTEGER datatype=<{0}>, <{1}>\n"), curr->cDataType, aStr);
              tmfflush(tmstdout);
          #endif
          /* Validate integer */
          if (*aStr && !isint(aStr))
            {
                retVal = 0;
                if (!*errmsg)
                    {
                      tmstrcat(errmsg, aStr);
                      tmstrcat(errmsg, _TMC(", invalid integer."));
                    }
              	tmstrcpy(aStr, _TMC("\0"));
                #ifdef SCT_DEBUG
                  tmprintf(&tmBundle, _TMC("aStr is not an INTEGER, set a Str to null <{0}>\n"), aStr);
                  tmfflush(tmstdout);
                #endif
           }
        }
   else
     if (tmstrcmp(curr->cDataType, _TMC("DOUBLE")) == 0)
        {
            ltrim(aStr, _TMC("0"));
            #ifdef SCT_DEBUG
                  tmprintf(&tmBundle, _TMC("aStr before validate DOUBLE datatype=<{0}>, <{1}>\n"), curr->cDataType, aStr);
                  tmfflush(tmstdout);
            #endif
          /* Validate number */
          if (*aStr && !isnum(aStr))
            {
                retVal = 0;
                if (!*errmsg)
                    {
                      tmstrcat(errmsg, aStr);
                      tmstrcat(errmsg, _TMC(", invalid number."));
                    }
              	tmstrcpy(aStr, _TMC("\0"));
                #ifdef SCT_DEBUG
                  tmprintf(&tmBundle, _TMC("aStr is not an DOUBLE, set a Str to null <{0}>\n"), aStr);
                  tmfflush(tmstdout);
                #endif
           }
        }
     else
        if (tmstrcmp(curr->cDataType, _TMC("DATE")) == 0)
            {
              /* Validate date */
            #ifdef SCT_DEBUG
              tmprintf(&tmBundle, _TMC("aStr before validate DATE datatype=<{0}>, <{1}>\n"), curr->cDataType, aStr);
              tmfflush(tmstdout);
            #endif
            if (*aStr && !isdate(aStr))
                {
                    retVal = 0;
                    if (!*errmsg)
                       {
                          tmstrcat(errmsg, aStr);
                          tmstrcat(errmsg, _TMC(", invalid date."));
                        }
                  	tmstrcpy(aStr, _TMC("\0"));
                    #ifdef SCT_DEBUG
                         tmprintf(&tmBundle, _TMC("aStr is not an DATE, set a Str to null <{0}>\n"), aStr);
                          tmfflush(tmstdout);
                    #endif
                }
             }

     if (!*aStr) {
      if (!*errmsg && (tmstrcmp(curr->cRequired, _TMC("Y")) == 0)) {
#ifdef SCT_DEBUG
  tmprintf(&tmBundle, _TMC("aStr is null and field is required<{0}>\n"), aStr);
  tmfflush(tmstdout);
#endif
        /* if aStr is null and field is required, set status to 1 and aStr to NULL */
        retVal = 0;
        tmstrcat(errmsg, curr->cName);
        tmstrcat(errmsg, _TMC(", field is required."));
        tmstrcpy(aStr, _TMC("\0"));
      }
    }

    if (!*aStr) {
#ifdef SCT_DEBUG
  tmprintf(&tmBundle, _TMC("aStr is null <{0}>\n"), aStr);
  tmfflush(tmstdout);
#endif
      tmstrcat(result, _TMC("NULL"));
#ifdef SCT_DEBUG
  tmprintf(&tmBundle, _TMC("  result <{0}>\n"), result);
  tmfflush(tmstdout);
#endif
    }
    else {
#ifdef SCT_DEBUG
  tmprintf(&tmBundle, _TMC("aStr is not null datatype<{0}>, <{1}>\n"), curr->cDataType, aStr);
  tmfflush(tmstdout);
#endif
      /* if datatype is integer/double, strcat as it is
         if datatype is char/date, prefix and postfix with single quote char */
      if ( (tmstrcmp(curr->cDataType, _TMC("INTEGER")) == 0)
      || (tmstrcmp(curr->cDataType, _TMC("DOUBLE")) == 0) ) {
        tmstrcat(result, aStr);
#ifdef SCT_DEBUG
  tmprintf(&tmBundle, _TMC("  INTEGER/DOUBLE result <{0}>\n"), result);
  tmfflush(tmstdout);
#endif
      }
      else if (tmstrcmp(curr->cDataType, _TMC("CHAR")) == 0) {
        tmstrcat(result, _TMC("'"));
        tmstrcat(result, aStr);
        tmstrcat(result, _TMC("'"));
#ifdef SCT_DEBUG
  tmprintf(&tmBundle, _TMC("  CHAR result <{0}>\n"), result);
  tmfflush(tmstdout);
#endif
      }
      else if (tmstrcmp(curr->cDataType, _TMC("DATE")) == 0) {
        tmstrcat(result, _TMC("TO_DATE('"));
        tmstrcat(result, aStr);
        tmstrcat(result, _TMC("','MM/DD/YYYY')"));
#ifdef SCT_DEBUG
  tmprintf(&tmBundle, _TMC("  DATE result <{0}>\n"), result);
  tmfflush(tmstdout);
#endif
      }
    }
#ifdef SCT_DEBUG
  tmprintf(&tmBundle, _TMC("  final result <{0}>\n"), result);
  tmfflush(tmstdout);
#endif


    i++;
    curr = curr->next;
  }

  tmstrcat(result, _TMC("\0"));

  if (retVal)
    tmstrcat(result, _TMC(",'0','No error.'"));
  else {
    tmstrcat(result, _TMC(",'1','"));
    tmstrcat(result, errmsg);
    tmstrcat(result, _TMC("'"));
  }

  free(aStr);

  return retVal;
}
/* ************************************************************************** */
static int load_file(TMCHAR *process_id, TMCHAR *file_name,
                     TMCHAR  *recIdList[], TMCHAR *table_name) {
  int ret_val;

  /* Select file definition from table PTRFILE */
  load_map_list(process_id, recIdList, &aMapList);

#ifdef SCT_DEBUG
  traverse_map_list(aMapList);
  tmfflush(tmstdout);
#endif

  /* Import the file to the temp table */
  ret_val = do_import(file_name, table_name);

  del_map_list(&aMapList);
  aMapList = NULL;

#ifdef SCT_DEBUG
  tmprintf(&tmBundle, _TMC("ret val END\n"));
  tmfflush(tmstdout);
#endif

  return ret_val;
}
/* ************************************************************************** */
static int do_import(TMCHAR *file_name, TMCHAR *table_name) {
  int retVal, recCount = 0;
  struct FileType *aFilePtr = NULL;
  TMCHAR *insStr = NULL;
  TMCHAR *colStr = NULL;
  TMCHAR *valStr = NULL;
  TMCHAR *sqlStr = NULL;
  TMCHAR8 stmt_c[3000];

#ifdef SCT_DEBUG
  tmprintf(&tmBundle, _TMC("do_import {0} ...  \n"), table_name);
  tmfflush(tmstdout);
#endif

  retVal = open_stream(file_name, &aFilePtr);

  /* if file is openned OK, process the file */
  if (retVal == 1) {
    colStr = (TMCHAR *) malloc(MAX_STR_LEN * tmcharsizeof(TMCHAR));
    valStr = (TMCHAR *) malloc(MAX_STR_LEN * tmcharsizeof(TMCHAR));
    insStr = (TMCHAR *) malloc(MAX_STR_LEN * tmcharsizeof(TMCHAR));
    sqlStr = (TMCHAR *) malloc(MAX_STR_LEN * tmcharsizeof(TMCHAR));

    tmmemset(sqlStr, '\0', tmcharsizeof(sqlStr));
    tmmemset(colStr, '\0', tmcharsizeof(colStr));
    tmmemset(valStr, '\0', tmcharsizeof(valStr));
    tmmemset(insStr, '\0', tmcharsizeof(insStr));

    /* Build column string */
    do_get_columns(aMapList, colStr);

#ifdef SCT_DEBUG
  tmprintf(&tmBundle, _TMC("done do_get_columns\n"));
  tmfflush(tmstdout);
#endif

    /* Build the insert statement */
    tmstrcpy(insStr, _TMC("INSERT INTO "));
    tmstrcat(insStr, table_name);
    tmstrcat(insStr, _TMC("("));
    tmstrcat(insStr, colStr);
    tmstrcat(insStr, _TMC(","));
    tmstrcat(insStr, table_name);
    tmstrcat(insStr, _TMC("_STATUS_TYPE,"));
    tmstrcat(insStr, table_name);
    tmstrcat(insStr, _TMC("_STATUS_DESC"));
    tmstrcat(insStr, _TMC(") VALUES("));

    /* Read file */
    do {
      /* Clear the string */
      tmstrcpy(&valStr[0], _TMC("\0"));
      tmstrcpy(&sqlStr[0], _TMC("\0"));
      tmstrcpy(&(aFilePtr->str)[0], _TMC("\0"));
      tmfgets(trim_space(aFilePtr->str), MAX_REC_LEN, aFilePtr->stream);

      /* Build value string */
      if (tmstrlen(aFilePtr->str) > 1) {
        recCount++;

#ifdef SCT_DEBUG
  tmprintf(&tmBundle, _TMC("aFilePtr->str <{0}>\n"), aFilePtr->str);
  tmfflush(tmstdout);
#endif
        do_get_values(aMapList, aFilePtr->str, valStr);

        tmstrcpy(sqlStr, insStr);
        tmstrcat(sqlStr, valStr);
        tmstrcat(sqlStr, _TMC(")"));
        tmstrcat(sqlStr, _TMC("\0"));

#ifdef SCT_DEBUG
  tmprintf(&tmBundle, _TMC("sqlStr = <{0}>\n"), sqlStr);
  tmfflush(tmstdout);
#endif

        tmstrcpy8(stmt_c, tmtochar8(sqlStr));
        EXEC SQL PREPARE S1 FROM :stmt_c;
        POSTORA;

        EXEC SQL EXECUTE IMMEDIATE :stmt_c;
        POSTORA;

        EXEC SQL COMMIT;
        POSTORA;
      }
    } while (!tmfeof(aFilePtr->stream));
    free(colStr);
    colStr = NULL;
    free(insStr);
    insStr = NULL;
    free(valStr);
    valStr = NULL;
    free(sqlStr);
    sqlStr = NULL;
  }

  retVal = recCount;
	close_stream(&aFilePtr);
  return retVal;
}
/* ************************************************************************** */
static TMCHAR * trim_space(TMCHAR *str) {
  ltrim(str, _TMC(" "));
  rtrim(str, _TMC(" "));

  return str;
}
/* *************************************************************************/
/* Replace all occurences of space with - */
static TMCHAR *replace_char(TMCHAR *str)
{
  TMCHAR *p;

  if ( !*str )
    return str;

  for ( p=str ; *p ; p++ )
    if ( tmstrchr(_TMC(" '"),*p) )
      *p = '-';
  return str;
}

/* *************************************************************************/
/*  Function sct_itoa
    The value is converted to a character string and returned.              */
/* *********************************************************************** */
TMCHAR *sct_itoa(int i) {
   static TMCHAR s[20];

   tmsprintf(&tmBundleCNum, s, _TMC("{0,number,integer}"), i);
#ifdef SCT_DEBUG
  tmprintf(&tmBundle, _TMC("sct_itoa i = [{0,number,integer}] to s = [{1}]\n"), i, s);
  tmfflush(tmstdout);
#endif

   return((TMCHAR *) s);
}
/* ************************************************************************** */
static int isint(TMCHAR *s) {
  int ret_val = YES;

  if (*s)
    ret_val = !tmstrcmp(sct_itoa(tmatoi(s)), s);
  return ret_val;
}
/* ************************************************************************** */
static int isnum(TMCHAR *s) {
  int ret_val = YES;
  TMCHAR lv_s [50] = {0};

  tmstrcpy(lv_s, s);
  if (*lv_s) {
    EXEC SQL EXECUTE
      DECLARE
 	      val  NUMBER;
  	    invalid_number EXCEPTION;
     	  PRAGMA Exception_Init (invalid_number, -1722);
      BEGIN
        SELECT TO_NUMBER(:lv_s)
        INTO   val
        FROM   DUAL;
      EXCEPTION
        WHEN invalid_number THEN
          :ret_val := 0;
      END;
    END-EXEC;
  }

  return ret_val;
}
/* ************************************************************************** */
static int isdate(TMCHAR *s) {
  int ret_val = YES;
  TMCHAR lv_s [50] = {0};

  tmstrcpy(lv_s, s);
  if (*lv_s) {
    EXEC SQL EXECUTE
      DECLARE
      	val  DATE;
      	e1 EXCEPTION;
      	e2 EXCEPTION;
      	e3 EXCEPTION;
      	e4 EXCEPTION;
      	e5 EXCEPTION;
      	e6 EXCEPTION;
      	e7 EXCEPTION;
      	e8 EXCEPTION;

  	    PRAGMA Exception_Init (e1, -1830);
          --ORA-01830: date format picture ends before converting entire input string
  	    PRAGMA Exception_Init (e2, -1839);
          --ORA-01839: date not valid for month specified
  	    PRAGMA Exception_Init (e3, -1840);
          --ORA-01840: input value not long enough for date format
  	    PRAGMA Exception_Init (e4, -1841);
          --ORA-01841: (full) year must be between -4713 and +9999, and not be 0
  	    PRAGMA Exception_Init (e5, -1843);
          --ORA-01843: not a valid month
  	    PRAGMA Exception_Init (e6, -1847);
          --ORA-01847: day of month must be between 1 and last day of month
  	    PRAGMA Exception_Init (e7, -1858);
          --ORA-01858: a non-numeric character was found where a numeric was expected
  	    PRAGMA Exception_Init (e8, -1861);
         --ORA-01861: literal does not match format string
      BEGIN
        SELECT to_date(:lv_s, 'MM/DD/YYYY')
        INTO   val
        FROM   DUAL;
      EXCEPTION
        WHEN e1 THEN
          :ret_val := 0;
        WHEN e2 THEN
          :ret_val := 0;
        WHEN e3 THEN
          :ret_val := 0;
        WHEN e4 THEN
          :ret_val := 0;
        WHEN e5 THEN
          :ret_val := 0;
        WHEN e6 THEN
          :ret_val := 0;
        WHEN e7 THEN
          :ret_val := 0;
        WHEN e8 THEN
          :ret_val := 0;
      END;
    END-EXEC;
  }

  return ret_val;
}
/* ************************************************************************** */
static int datecmp(TMCHAR *d1, TMCHAR *d2) {
  int retVal;
  TMCHAR lv_d1 [50] = {0};
  TMCHAR lv_d2 [50] = {0};
  tmstrcpy(lv_d1, d1);
  tmstrcpy(lv_d2, d2);


  EXEC SQL EXECUTE
    DECLARE
      date1 DATE;
      date2 DATE;
    BEGIN
      SELECT TO_DATE(:lv_d1, 'MM/DD/YYYY'), TO_DATE(:lv_d2, 'MM/DD/YYYY')
      INTO date1, date2
      FROM DUAL;

      IF date1 < date2 THEN
        :retVal := -1;
      ELSIF date1 = date2 THEN
        :retVal := 0;
      ELSE
        :retVal := 1;
      END IF;
    END;
  END-EXEC;
  POSTORA;

  return retVal;
}
/******************************* END OF SOURCE *******************************/
